# Java sdk安装教程

## 一、下载地址

[Java Downloads | Oracle](https://www.oracle.com/java/technologies/downloads/#java8)

## 二、 选择下载版本

选择Java Sdk版本

![image-20231008090424478](<../../.gitbook/assets/image 20231008090424478.png>)

点击确认合约

![image-20231008090448848](<../../.gitbook/assets/image 20231008090448848.png>)

下载需要登录甲骨文云

![image-20231008090510330](<../../.gitbook/assets/image 20231008090510330.png>)

没有的直接创建，没有公司可以随便写

![image-20231008090552463](<../../.gitbook/assets/image 20231008090552463.png>)

登录完就可以下载了

## 三、安装

![安装文件](<../../.gitbook/assets/image 20231008192532846.png>)

![安装位置](<../../.gitbook/assets/image 20231008192913254.png>)

![](<../../.gitbook/assets/image 20231008193017734.png>)

![image-20231008193043216](<../../.gitbook/assets/image 20231008193043216.png>)

## 四、添加环境变量

1. 选择我的电脑右键打开属性

![](<../../.gitbook/assets/image 20231008193155182.png>)

2. 选择环境变量

![](<../../.gitbook/assets/image 20231008193213816.png>)

3. 把安装的Java jdk路径放到环境变量里面

![](<../../.gitbook/assets/image 20231008193249308.png>)
