# Tomcat服务器安装

***

## 1.下载

### 1.1打开官网

> [tomcat](https://tomcat.apache.org/)

![TomCat版本页](<../../.gitbook/assets/image 20231011085427904.png>)

### 1.2找到服务器版本

> 在侧边栏选择自己需要的版本

![选择版本](<../../.gitbook/assets/image 20231011085515740.png>)

### 1.3 选择下载

> 下载自己系统的对应

![选择下载状态](<../../.gitbook/assets/image 20231011090727104.png>)

### 1.4 解压到存放盘

> 找到你的存放位置

![](<../../.gitbook/assets/image 20231011091348447.png>)

## 2.配置

### 2.1打开环境变量

> 按win键输入 **env**

![](<../../.gitbook/assets/image 20231011092244827.png>)

### 2.2 新建环境变量

> 变量名为Java\_home
>
> 变量值为jdk的根目录
>
> :bulb: :不能是jdk的bin目录

![](<../../.gitbook/assets/image 20231011092414191.png>)

## 3.启动服务器

### 3.1 启动脚本

> 打开Tomcat目录下面的bin的startup.bat文件

![image-20231011092556437](<../../.gitbook/assets/image 20231011092556437.png>)

> 双击

![image-20231011092625154](<../../.gitbook/assets/image 20231011092625154.png>)

### 3.2 在浏览器打开

> 地址栏输入`http://localhost:8080/`

![](<../../.gitbook/assets/image 20231011092726672.png>)

出现以上页面就安装🆗了，就可以进行实例开发了

### 3.3 命令启动

> 找到tomcat的目录添加环境变量

![](<../../.gitbook/assets/image 20231017102024040.png>)

![](<../../.gitbook/assets/image 20231017102635965.png>)

输入命令 `startup.bat`

![image-20231017102546642](<../../.gitbook/assets/image 20231017102546642.png>)

`shutdown.bat`

![image-20231017104158740](<../../.gitbook/assets/image 20231017104158740.png>)

## 4. TomCat的目录介绍

> 其中，Tomcat将由JSP文件转译后的Java源文件和class文件存放在work文件夹下
>
> bin为Tomcat执行脚本目录
>
> conf文件夹下存放有Tomcat的配置文件
>
> lib文件夹为Tomcat运行时需要的库文件
>
> Tomcat执行时的日志文件存放在logs文件夹下
>
> webapps为Tomcat的Web发布目录。

![](<../../.gitbook/assets/image 20231017105102158.png>)

## 5. 创建运行第一个JSP程序

（1）在Tomcat安装目录下的Webapps目录中，可以看到ROOT、examples、manager、 tomcat-docs之类Tomcat自带的Web应用范例。

（2）在webapps目录下新建一个名称为HelloJsp的文件夹。

（3）在HelloJsp下新建一个文件夹Web-INF。注意，目录名称是区分大小写的。

![](<../../.gitbook/assets/image 20231017105314996.png>)

![新建HelloJsp](<../../.gitbook/assets/image 20231017110311606.png>)

（4）在Web-INF下新建一个文件web.xml，该文件为Tomcat的部署文件，并在其中添加如下代码：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd">
<web-app>
      <display-name>My Web Application</display-name>
     <description>
           A JSP application for test
     </description>
     <welcome-file-list>
           <welcome-file>Test.jsp</welcome-file>
     </welcome-file-list>
</web-app>
```

（5）在HelloJsp目录下创建文本文件，并为其指定文件名为Test.jsp。注意JSP页面的扩展名必须为.jsp。然后在该文本文件中输入如下代码：

```java
<%@ page contentType="text/html; charset=gb2312" %>
<html>
   <head>
      <title>
           第一个JSP程序
      </title>
   </head>
   <body>
       <h2 align="center">
           <%=new java.util.Date()%>
       </h2>
   </body>
</html>
```

（6）启动Tomcat，当Tomcat重新启动时会自动部署webapps下的所有Web应用。

（7）打开浏览器，输入http://localhost:8080/HelloJsp/Test.jsp

<figure><img src="../../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>
